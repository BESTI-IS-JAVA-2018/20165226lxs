import java.util.*;
class StudentKey implements Comparable {
    double d=0;
    long n=0;
    StudentKey (double d) {
        this.d=d;
    }
    StudentKey (long n){
        this.n=n;
    }
    public int compareTo(Object b) {
        StudentKey st=(StudentKey)b;
        if((this.d-st.d)==0)
            return -1;
        else
            return (int)((this.d-st.d)*1000);
    }
}
class Student {
    String name=null;
    double math,computer,english,total;
    String id;
    Student(String r,String s,double m,double e,double b,double y) {
        id=r;
        name=s;
        math=m;
        english=e;
        computer=b;
        total=y;
    }
}
public class paixu {
    public static void main(String args[ ]) {
        TreeMap<StudentKey,Student>  treemap= new TreeMap<StudentKey,Student>();
        String str[]={"陆艺杰","王高源","刘香杉","朱越","赵凯杰"};
        String id[]={"20165224","20165225","20165226","20165227","20165228"};
        double math[]={89,45,88,78,76};
        double english[]={67,66,90,90,56};
        double computer[]={78,87,88,98,89};
        Student student[]=new Student[5];
        double total[]={234,198,266,266,221};
        for(int k=0;k<student.length;k++) {
            student[k]=new Student(id[k],str[k],math[k],english[k],computer[k],total[k]);
        }

        StudentKey key[]=new StudentKey[5] ;
        for(int k=0;k<key.length;k++) {
            key[k]=new StudentKey(student[k].total);                                           //关键字按总成绩排列大小
        }
        for(int k=0;k<student.length;k++) {
            treemap.put(key[k],student[k]);
        }
        int number=treemap.size();
        System.out.println("树映射中有"+number+"个对象,按总成绩排序:");
        Collection<Student> collection=treemap.values();
        Iterator<Student> iter=collection.iterator();
        while(iter.hasNext()) {
            Student stu=iter.next();
            System.out.println("姓名 "+stu.name+" 学号 "+stu.id+" 数学 "+stu.math+" 英语 "+stu.english+" 计算机 "+stu.computer+" 总成绩 "+stu.total);
        }
        treemap.clear();
        for(int k = 0; k<key.length; k++) {
            key[k]=new StudentKey(Double.parseDouble(student[k].id));                             //关键字按学号排列大小
        }
        for(int k=0;k<student.length;k++) {
            treemap.put(key[k],student[k]);
        }
        number=treemap.size();
        System.out.println("树映射中有"+number+"个对象:按学号排序:");
        collection=treemap.values();
        iter=collection.iterator();
        while(iter.hasNext()) {
            Student stu=(Student)iter.next();
            System.out.println("姓名 "+stu.name+" 学号 "+stu.id+" 数学 "+stu.math+" 英语 "+stu.english+" 计算机 "+stu.computer+" 总成绩 "+stu.total);
        }
    }
}

