import java.util.*;
public class C {
    public static void main(String args[]) {
        int [] temp = new int [args.length];
        for(int i=0; i<args.length;i++) {
            temp[i] = Integer.parseInt(args[i]);
        } 
        System.out.println(fact(temp[0],temp[1]));
        }
        public static int fact(int n , int m) {
            if(m==1) return n;
            else  if( m==0 ||  m==n) return 1;
            else  if(n<m || n==0) return 0;
            else
                return fact(n-1, m-1)+fact(n-1,m);
        }
 }
