
import java.util.*;
public class E2 {
    public static void main(String[] args) {
        Stack<Character>mystack1=new Stack<Character>(),
                mystack2=new Stack<Character>();
        StringBuffer bu=new StringBuffer();
        for(char c='A';c<='D';c++){
            mystack1.push(new Character(c));
        }
        while(!(mystack1.empty())) {
            Character temp=mystack1.pop();
            mystack2.push(temp);
        }
        while (!(mystack2.empty())) {
            Character temp=mystack2.pop();
            bu.append(temp.charValue());
        }
        System.out.println(bu);
    }
}
