import java.lang.String;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class StudentTest {
    class Stu{
        public int age;
        public String name;
        public int id;
        public int english_score;
        public int computer_score;
        public int maths_score;
        public int total_score;

        public Stu(int id, String name,int english_score,int computer_score,int maths_score,int total_score) {
            super();
            this.id = id;
            this.name = name;
            this.english_score = english_score;
            this.computer_score = computer_score;
            this.maths_score = maths_score;
            this.total_score = total_score;
        }
        @Override
        public String toString() {
            return ( "\n"+" 学号 " + id + " 姓名 " + name +" 英语 "+english_score+" 计算机 "+computer_score+" 数学 "+maths_score+" 总成绩 "+total_score);
        }
    }
    public static void main(String[] args) {
        List<Stu> list= new ArrayList<>();
        list.add(new StudentTest().new Stu(20165224, "陆艺杰",89,67,78,234));
        list.add(new StudentTest().new Stu(20165227, "朱越",78,90,98,266));
        list.add(new StudentTest().new Stu(20165226, "刘香杉",88,90,88,266));
        list.add(new StudentTest().new Stu(20165225, "王高源",45,66,87,198));
        list.add(new StudentTest().new Stu(20165228, "苏祚堃",76,56,89,221));
        System.out.println("排序前："+list);
        Collections.sort(list, new Comparator<Stu>() {

            @Override
            public int compare(Stu o1, Stu o2) {
                return o1.id - o2.id;
            }
        });
        System.out.println("按照学号排序："+list);
        Collections.sort(list, new Comparator<Stu>() {

            @Override
            public int compare(Stu o1, Stu o2) {
                return o1.total_score - o2.total_score;
            }
        });
        System.out.println("按总成绩顺序排序："+list);
    }
}
