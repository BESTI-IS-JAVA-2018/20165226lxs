/**
 * Created by xiang on 2018/5/6.
 */

public class student {
    private char sex;
    private String id;//表示学号
    private String name;//表示姓名
    private int age;//表示年龄
    private double computer_score;//表示计算机课程的成绩
    private double english_score;//表示英语课的成绩
    private double maths_score;//表示数学课的成绩
    private double total_score;// 表示总成绩
    private double ave_score; //表示平均成绩

    public student(String id, String name){
        this.id = id;
        this.name = name;
    }
    public student(String id, String name, char sex, int age){
        this(id, name);
        this.sex = sex;
        this.age = age;
    }
    public String getId(){
        return id;
    }//获得当前对象的学号，
    public double getComputer_score(){
        return computer_score;
    }//获得当前对象的计算机课程成绩,
    public double getMaths_score(){
        return maths_score;
    }//获得当前对象的数学课程成绩,
    public double getEnglish_score(){
        return english_score;
    }//获得当前对象的英语课程成绩,

    public void setId(String id){
        this.id=id;
    }// 设置当前对象的id值,
    public void setComputer_score(double computer_score){
        this.computer_score=computer_score;
    }//设置当前对象的Computer_score值,
    public void setEnglish_score(double english_score){
        this.english_score=english_score;
    }//设置当前对象的English_score值,
    public void setMaths_score(double maths_score){
        this.maths_score=maths_score;
    }//设置当前对象的Maths_score值,

    public double total_score(){
        return computer_score+maths_score+english_score;
    }// 计算Computer_score, Maths_score 和English_score 三门课的总成绩。

}



