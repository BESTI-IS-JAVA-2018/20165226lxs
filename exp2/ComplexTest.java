import junit.framework.TestCase;
import org.junit.Test;
/**
 * Created by xiang on 2018/4/15.
 */
public class ComplexTest extends TestCase {
    public void testComplexAdd() throws Exception {
     assertEquals(5,2+3);
    }

    public void testComplexSub() throws Exception {
    assertEquals(6,9 - 3);
    }

    public void testComplexMulti() throws Exception {
    assertEquals(6,2 * 3);
    }

    public void testComplexDiv() throws Exception {
    assertEquals(2,6 / 3);
    }

}