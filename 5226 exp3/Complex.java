class Complex {
    // 定义属性并生成getter,setter
    private double RealPart;
    private double ImagePart;
    // 定义构造函数
    public Complex(){

    }
    public Complex(double R, double I){
        this.RealPart = R;
        this.ImagePart = I;
    }

    public double getRealPart() {
        return RealPart;
    }

    public void setRealPart(double realPart) {
        RealPart = realPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    public void setImagePart(double imagePart) {
        ImagePart = imagePart;
    }

    //Override Object
    public boolean equals(Object obj){
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof Complex)) {
            return false;
        }
        Complex complex = (Complex) obj;
        if(complex.RealPart != ((Complex) obj).RealPart) {
            return false;
        }
        if(complex.ImagePart != ((Complex) obj).ImagePart) {
            return false;
        }

        return true;
    }
    public String toString()   {
        String string = "";
        if (ImagePart > 0)
            string =  RealPart + "+" + ImagePart + "i";
        if (ImagePart == 0)
            string =  RealPart + "";
        if (ImagePart < 0)
            string = RealPart + " " + ImagePart + "i";
        return string;
    }

    // 定义公有方法:加减乘除
    Complex ComplexAdd(Complex a) {
        return  new Complex(RealPart+a.RealPart,ImagePart+a.ImagePart);
    }
    Complex ComplexSub(Complex a) {
        return new Complex(RealPart-a.RealPart,ImagePart-a.ImagePart);
    }
    Complex ComplexMulti(Complex a) {
        return new Complex(RealPart*a.RealPart-ImagePart*a.ImagePart,ImagePart*a.RealPart+RealPart*a.ImagePart);
    }
    Complex  ComplexDiv(Complex a) {
        Complex d = new Complex();
        d.RealPart = (this.RealPart * a.RealPart + this.ImagePart * a.ImagePart)/(a.RealPart*a.RealPart+a.ImagePart*a.ImagePart);
        d.ImagePart = (this.ImagePart * a.RealPart - this.RealPart * a.ImagePart)/(a.RealPart*a.RealPart+a.ImagePart*a.ImagePart);
        return d;
    }

}
