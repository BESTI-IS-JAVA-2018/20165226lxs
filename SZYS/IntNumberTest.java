import junit.framework.TestCase;
public class IntNumberTest extends TestCase {

    public void testAdd () {
        assertEquals(20, 10 + 10);
        assertEquals(0,-10 + 10);
        assertEquals(-10,-5 + (-5));
    }

    public void testSubtract () {
        assertEquals(10, 20 - 10);
        assertEquals(0,10 - 10);
        assertEquals(-10,0 - 10);
    }

    public void testMultiply () {
        assertEquals(100, 25 * 4);
        assertEquals(0,0 * 0);
        assertEquals(-100,25 * (-4));
    }

    public void testDivide () {
        assertEquals(10, 100 / 10);
        assertEquals(0,0 / 10);
        assertEquals(-10,100/ (-10));
    }


}
