import javax.lang.model.element.NestingKind;
import java.util.*;

public class MultiLanguage {
    private Integer option;
    private String language;
    public String getLanguage() {
        return language;
    }
    public MultiLanguage(){
        setLanguage();
    }
    private void setLanguage(){
        System.out.printf("Please choose a language and type it! \n" +
                "请你选择一种语言并且输入对应的缩写！\n" +
                "請妳選擇壹種語言並且輸入對應的縮寫！\n" +
                "EN/ZH_CN/ZH_HK");
        HashMap<String,Integer> lanList = new HashMap<String,Integer>();
        lanList.put("EN",0);
        lanList.put("ZH_CN",1);
        lanList.put("ZH_HK",2);
        Scanner sc = new Scanner(System.in);
        language = sc.nextLine();
        this.option = lanList.get(language);
    }
    public void output(String type){
        String[] output_1 = {"Ready......Go!","预备，开始！","準備，開始！"},
                output_2 = {"YOUR ANSWER : ","你的回答是：","妳的回答是："},
                output_3 = {"KEY :","答案为：","答案为："},
                output_4 = {"------------ERROR------------","------------错误------------","------------錯誤------------"},
                output_5 = {"------------ACCESS-----------","------------通过-------------","------------通過------------"},
                output_6 = {"Your Accuracy:","您的正确率：","您的正確率："};

        if(type.equals("st"))System.out.printf(output_1[option] + "\n");
        else if(type.equals("ans"))System.out.printf(output_2[option]);
        else if(type.equals("key"))System.out.printf(output_3[option]);
        else if(type.equals("err"))System.out.printf(output_4[option] + "\n");
        else if(type.equals("acc"))System.out.printf(output_5[option] + "\n");
        else if (type.equals ( "accuracy") )System.out.printf(output_6[option] );

    }
}
